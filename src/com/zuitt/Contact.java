package com.zuitt;

import java.util.ArrayList;

public class Contact {

    private String name;
    private ArrayList<String> phoneNumber = new ArrayList<String>();
    private ArrayList<String> address = new ArrayList<String>();


    public Contact() {}
    public Contact(String name, ArrayList <String> phoneNumber, ArrayList <String> address) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.address = address;
    }

    public String getName(){
        return name;
    }
    public ArrayList <String> getPhoneNumber(){
        return phoneNumber;
    }
    public ArrayList <String>getAddress(){
        return address;
    }

    public void setName (String name) {
        this.name = name;
    }

    public void setPhoneNumber ( ArrayList phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public void setAddress ( ArrayList address) {
        this.address = address;
    }



}
