package com.zuitt;

import java.util.ArrayList;

public class Phonebook {

    ArrayList<String> contacts = new ArrayList<String>();


    public Phonebook(){}

    public Phonebook(ArrayList<String> contacts) {
        this.contacts = contacts;
    }

    public ArrayList getContacts(){
        return contacts;
    }
    public void setContacts ( ArrayList contacts) {
        this.contacts = contacts;
    }

}
